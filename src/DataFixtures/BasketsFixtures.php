<?php

namespace App\DataFixtures;

use App\Entity\Baskets;
use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class BasketsFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Undocumented function
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 1000; $i++) {
            $baskets = new Baskets();
            $user = $this->getReference('user_' . $i);

            $baskets->setAmountToPay($faker->randomFloat(null, 10000, 50000))
                ->setSlug($faker->md5(uniqid()))
                ->setUser($user)
                ->setDescription('Description N° ' . $i);

            $this->addReference('baskets_' . $i, $baskets);
            $manager->persist($baskets);
        }

        $manager->flush();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
