<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Remark;
use App\DataFixtures\UserFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class RemarkFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Undocumented function
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 1000; $i++) {
            $remark = new Remark();
            $blogpost = $this->getReference('blog_' . $faker->numberBetween(0, 999));
            $user = $this->getReference('user_' . $faker->numberBetween(0, 999));

            $remark->setUser($user)
                ->setBlogpost($blogpost)
                ->setCommentOn($faker->dateTimeBetween('-6 day', 'now', null))
                ->setContents($faker->text());

            $this->addReference('remark_' . $i, $remark);
            $manager->persist($remark);
        }

        $manager->flush();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            BlogpostFixtures::class,
        ];
    }
}
