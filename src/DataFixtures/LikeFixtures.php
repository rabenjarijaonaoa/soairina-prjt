<?php

namespace App\DataFixtures;

use App\Entity\Like;
use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class LikeFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Undocumented function
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 10000; $i++) {
            $like = new Like();
            $blogpost = $this->getReference('blog_' . $faker->numberBetween(0, 999));
            $user = $this->getReference('user_' . $faker->numberBetween(0, 999));
            $product = $this->getReference('product_' . $faker->numberBetween(0, 999));

            $like->setBlogpost($blogpost)
                ->setProduct($product)
                ->setAuthor($user);

            $this->addReference('like_' . $i, $like);
            $manager->persist($like);
        }

        $manager->flush();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            BlogpostFixtures::class,
            ProductFixtures::class,
        ];
    }
}
