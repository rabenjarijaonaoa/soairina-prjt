<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use DateTime;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /** object $encoder */
    private $encoder;

    /**
     * Construct.
     *
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    /**
     * Undocumented function
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 1000; $i++) {
            $user = new User();
            $gender = $faker->randomElement(['male', 'female']);
            $lastname = $faker->lastName;
            $firstname = $faker->firstName($gender);
            $user->setEmail(strtolower($firstname)  . "." . strtolower($lastname) . rand(0, 1000) . "@" . $faker->randomElement(['gmail', 'yahoo', 'outlook', 'hotmail']) . "." . $faker->randomElement(['com', 'fr', 'net', 'org']))
                ->setFirstname($firstname)
                ->setLastname($lastname)
                ->setAdress($faker->address())
                ->setPhoneNumber($faker->phoneNumber())
                ->setCreatedAt(new \DateTime('now'));

            $password = $this->encoder->encodePassword($user, '1234@Test');
            $user->setPassword($password);

            if ($gender === 'male') {
                $user->setPictures("male.png");
            } else {
                $user->setPictures("female.png");
            }

            if ($i === 0) {
                $user->setRoles(['ROLE_ADMIN']);
            } else {
                $user->setRoles(['ROLE_USER']);
            }

            $this->addReference('user_' . $i, $user);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
