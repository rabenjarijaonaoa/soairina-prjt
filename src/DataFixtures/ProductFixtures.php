<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Product;
use App\DataFixtures\BlogpostFixtures;
use App\DataFixtures\CategoryFixtures;
use App\Entity\Baskets;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Undocumented function
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 1000; $i++) {
            $product = new Product();

            $category = $this->getReference('category_' . $faker->numberBetween(0, 23));
            $blogpost = $this->getReference('blog_' . $faker->numberBetween(0, 999));

            $product->setName('Produit N° '. $i)
                ->setDescription('Décription N° ' .$i)
                ->setSlug($faker->md5(uniqid()))
                ->setPrice($faker->randomFloat(null, 10000, 50000))
                ->setColor($faker->hexcolor())
                ->setNumberAvalaible($faker->numberBetween(0, 50))
                ->setCreatedAt($faker->dateTimeBetween('-6 day', 'now', null))
                ->setCategory($category)
                ->setBlogpost($blogpost);

            $this->addReference('product_' . $i, $product);

            for ($j = 0; $j < mt_rand(1, 10); $j++) {
                $size = $this->getReference('size_' . $faker->numberBetween(0, 6));
                $product->addSize($size);
            }

            for ($j = 0; $j < mt_rand(1, 10); $j++) {
                $type = $this->getReference('type_' . $faker->numberBetween(0, 1));
                $product->addType($type);
            }

            for ($j = 0; $j < mt_rand(1, 10); $j++) {
                $image = $this->getReference('image_' . $faker->numberBetween(0, 999));
                $product->addImage($image);
            }

            for ($j = 0; $j < mt_rand(1, 10); $j++) {
                $baskets = $this->getReference('baskets_' . $faker->numberBetween(0, 999));
                $product->addBasket($baskets);
            }

            $manager->persist($product);
        }

        $manager->flush();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            BlogpostFixtures::class,
            SizesFixtures::class,
            TypeFixtures::class,
            ImageFixtures::class,
            BasketsFixtures::class,
        ];
    }
}
