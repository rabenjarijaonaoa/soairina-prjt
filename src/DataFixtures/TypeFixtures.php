<?php

namespace App\DataFixtures;

use App\Entity\Type;
use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class TypeFixtures extends Fixture
{
    /**
     * Undocumented function
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $listOfType = ['COTON', 'POLYCOTON'];

        for ($i = 0; $i < count($listOfType); $i++) {
            $type = new Type();

            $type->setName($listOfType[$i]);

            $this->addReference('type_' . $i, $type);
            $manager->persist($type);
        }

        $manager->flush();
    }
}
