<?php

namespace App\DataFixtures;

use App\Entity\Blogpost;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class BlogpostFixtures extends Fixture
{
    /**
     * Undocumented function
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 1000; $i++) {
            $blog = new Blogpost();
            $blog->setTitle('Publication N° ' . $i)
                ->setContents('Contenu N° ' . $i)
                ->setPublishedOn($faker->dateTimeBetween('-6 day', 'now', null))
                ->setSlug($faker->md5(uniqid()));

            $this->addReference('blog_' . $i, $blog);
            $manager->persist($blog);
        }
        $manager->flush();
    }
}
