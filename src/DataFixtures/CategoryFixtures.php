<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class CategoryFixtures extends Fixture
{
    /**
     * Undocumented function
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $ctg = ['Tee-shirt', 'Polo', 'Body', 'Jogging', 'Sweatshirt', 'Maillot', 'Survêtement', 'Pantalon', 'Short', 'Pyjama', 'Chemise', 'Robe', 'Jupe', 'Sac', 'Trousse', 'Cartable', 'Casquette', 'Bob', 'Vêtement de travail', 'Nappe', 'Sous plat', 'Drap', 'Serviette', 'Divers articles'];

        for ($i = 0; $i < count($ctg); $i++) {
            $category = new Category();

            $category->setName($ctg[$i])
                ->setDescription('Coton - Polycoton')
                ->setSlug($faker->md5(uniqid()));

            $this->addReference('category_' . $i, $category);
            $manager->persist($category);
        }

        $manager->flush();
    }
}
