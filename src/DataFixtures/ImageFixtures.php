<?php

namespace App\DataFixtures;

use App\Entity\Image;
use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ImageFixtures extends Fixture
{
    /**
     * Undocumented function
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 1000; $i++) {
            $image = new Image();

            $image->setUrl('produit' . rand(1, 6) . '.jpg')
                ->setTitle('Image N° ' . $i)
                ->setSlug($faker->md5(uniqid()))
                ->setCreatedAt(new \DateTime('now'));

            $this->addReference('image_' . $i, $image);

            $manager->persist($image);
        }

        $manager->flush();
    }
}
