<?php

namespace App\DataFixtures;

use App\Entity\Sizes;
use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class SizesFixtures extends Fixture
{
    /**
     * Undocumented function
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $listOfSize = ['2XS', 'XS', 'S', 'M', 'L', 'XL', '2XL'];
        for ($i = 0; $i < sizeof($listOfSize); $i++) {
            $size = new Sizes();
            $size->setName($listOfSize[$i]);

            $this->addReference('size_' . $i, $size);
            $manager->persist($size);
        }
        $manager->flush();
    }
}
