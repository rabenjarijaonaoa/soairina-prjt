<?php

namespace App\Entity;

use App\Repository\BlogpostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BlogpostRepository::class)
 */
class Blogpost
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contents;

    /**
     * @ORM\Column(type="datetime")
     */
    private $publishedOn;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="blogpost")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=Like::class, mappedBy="blogpost")
     */
    private $likes;

    /**
     * @ORM\OneToMany(targetEntity=Remark::class, mappedBy="blogpost")
     */
    private $remark;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->remark = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContents(): ?string
    {
        return $this->contents;
    }

    public function setContents(string $contents): self
    {
        $this->contents = $contents;

        return $this;
    }

    public function getPublishedOn(): ?\DateTimeInterface
    {
        return $this->publishedOn;
    }

    public function setPublishedOn(\DateTimeInterface $publishedOn): self
    {
        $this->publishedOn = $publishedOn;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setBlogpost($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getBlogpost() === $this) {
                $product->setBlogpost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Like[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Like $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setBlogpost($this);
        }

        return $this;
    }

    public function removeLike(Like $like): self
    {
        if ($this->likes->removeElement($like)) {
            // set the owning side to null (unless already changed)
            if ($like->getBlogpost() === $this) {
                $like->setBlogpost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Remark[]
     */
    public function getRemark(): Collection
    {
        return $this->remark;
    }

    public function addRemark(Remark $remark): self
    {
        if (!$this->remark->contains($remark)) {
            $this->remark[] = $remark;
            $remark->setBlogpost($this);
        }

        return $this;
    }

    public function removeRemark(Remark $remark): self
    {
        if ($this->remark->removeElement($remark)) {
            // set the owning side to null (unless already changed)
            if ($remark->getBlogpost() === $this) {
                $remark->setBlogpost(null);
            }
        }

        return $this;
    }
}
