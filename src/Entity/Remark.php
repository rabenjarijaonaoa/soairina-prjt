<?php

namespace App\Entity;

use App\Repository\RemarkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RemarkRepository::class)
 */
class Remark
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contents;

    /**
     * @ORM\Column(type="datetime")
     */
    private $commentOn;

    /**
     * @ORM\ManyToOne(targetEntity=Blogpost::class, inversedBy="remark")
     */
    private $blogpost;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="remark")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContents(): ?string
    {
        return $this->contents;
    }

    public function setContents(string $contents): self
    {
        $this->contents = $contents;

        return $this;
    }

    public function getCommentOn(): ?\DateTimeInterface
    {
        return $this->commentOn;
    }

    public function setCommentOn(\DateTimeInterface $commentOn): self
    {
        $this->commentOn = $commentOn;

        return $this;
    }

    public function getBlogpost(): ?Blogpost
    {
        return $this->blogpost;
    }

    public function setBlogpost(?Blogpost $blogpost): self
    {
        $this->blogpost = $blogpost;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function __toString(): string
    {
        return $this->contents;
    }
}
