<?php

namespace App\Repository;

use App\Entity\Like;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Like|null find($id, $lockMode = null, $lockVersion = null)
 * @method Like|null findOneBy(array $criteria, array $orderBy = null)
 * @method Like[]    findAll()
 * @method Like[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LikeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Like::class);
    }

    public function getBestLikeBlogpost()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT *, count(*) Total FROM `like` GROUP BY `blogpost_id` ORDER BY Total DESC LIMIT 1";
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->execute();

        return $resultSet->fetchAll();
    }
}
