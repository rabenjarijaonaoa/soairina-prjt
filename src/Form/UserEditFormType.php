<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class UserEditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pictures', FileType::class, [
                'required' => false,
                'data_class' => null,
            ])
            ->add('firstname', TextType::class, [
                'required' => false,
                'constraints' => [],
            ])
            ->add('lastname', TextType::class, [
                'required' => false,
                'constraints' => [],
            ])
            ->add('email', EmailType::class, [
                'required' => false,
                'invalid_message' => 'Email invalide.',
                'constraints' => [],
            ])
            ->add('phoneNumber', TextType::class, [
                'required' => false,
                'constraints' => [],
            ])
            ->add('adress', TextType::class, [
                'required' => false,
                'constraints' => [],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
