<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Regex;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pictureFile', VichImageType::class, [
                'label' => false,
                'required' => false,
                'allow_delete' => false,
            ])
            ->add('firstname', TextType::class, [
                'required' => false,
                'constraints' => [],
            ])
            ->add('lastname', TextType::class, [
                'required' => false,
                'constraints' => [],
            ])
            ->add('email', EmailType::class, [
                'required' => false,
                'invalid_message' => 'Email invalide.',
                'constraints' => [],
            ])
            ->add('phoneNumber', TextType::class, [
                'required' => false,
                'constraints' => [],
            ])
            ->add('adress', TextType::class, [
                'required' => false,
                'constraints' => [],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les champs de mot de passe doivent correspondre.',
                'required' => false,
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new Regex([
                        'pattern' => "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*-])[A-Za-z\d!@#$%^&*-]{8,}$/",
                        'message' => 'Le Mot de passe doit contenir au moins 8 caractères, une majuscule, un chiffre et au moins un de ces caractères spéciaux (!@#$%^&*-)',
                    ]),
                    new NotBlank([
                        'message' => 'Veuillez renseigner ce champ.'
                    ])
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
