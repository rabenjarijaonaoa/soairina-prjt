<?php

namespace App\Controller;

use App\Classe\Search;
use App\Entity\Product;
use App\Form\SearchType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    #[Route('/product', name: 'product')]
    public function index(
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository(Product::class)->findAllOrderByCreatedAt();

        $search = new Search();
        $form = $this->createForm(SearchType::class, $search);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $products = $em->getRepository(Product::class)->findAllWithSearch($search);
        }

        $data = $paginator->paginate(
            $products,
            $request->query->getInt('page', 1),
            12
        );

        return $this->render('product/index.html.twig', [
            'products' => $data,
            'formSearch' => $form->createView(),
        ]);
    }

    /**
     * @Route("/product/{id}", name="product-id")
     */
    public function FunctionName($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository(Product::class)->findOneBy(['id' => $id]);

        return $this->render('product/product.html.twig', [
            'product' => $data,
        ]);
    }
}
