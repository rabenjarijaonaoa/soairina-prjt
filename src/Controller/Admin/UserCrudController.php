<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $roles = [
            'User'   => 'ROLE_USER',
            'Admin'  => 'ROLE_ADMIN'
        ];

        return [
            IdField::new('id')->hideOnForm()->hideOnIndex(),
            TextField::new("firstname"),
            TextField::new("lastname"),
            EmailField::new("email"),
            TextField::new("password")->setFormType(PasswordType::class)->hideOnIndex(),
            TextField::new("phoneNumber"),
            TextField::new("adress"),
            ChoiceField::new("roles")->setChoices($roles)->allowMultipleChoices(),
            BooleanField::new("isVerified"),
            DateTimeField::new("createdAt")->hideOnForm(),
            DateTimeField::new("updateAt")->hideOnForm(),
            TextareaField::new("pictureFile")->setFormType(VichImageType::class)->onlyOnForms(),
            ImageField::new("pictures")->setUploadDir('/public/')->setBasePath('/uploads/image/user')->hideOnForm(),
        ];
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        if (!$entityInstance instanceof User) return;

        $entityInstance->setCreatedAt(new \DateTime());

        parent::persistEntity($entityManager,$entityInstance);
    }

}
