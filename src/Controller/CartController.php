<?php

namespace App\Controller;

use App\Classe\Cart;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/cart', name: 'cart_')]
class CartController extends AbstractController
{
    #[Route('/', name: 'view')]
    public function index(Cart $cart): Response
    {
        $data = $cart->getFull();

        return $this->render('cart/index.html.twig', [
            'cart' => $data
        ]);
    }

    #[Route('/add/{id}', name: 'add')]
    public function add(Cart $cart, $id): Response
    {
        $cart->add($id);
        return $this->redirectToRoute('cart_view');
    }

    #[Route('/remove', name: 'remove')]
    public function remove(Cart $cart): Response
    {
        $cart->remove();
        return $this->redirectToRoute('cart_view');
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function delete(Cart $cart, $id): Response
    {
        $cart->delete($id);
        return $this->redirectToRoute('cart_view');
    }

    #[Route('/decrease/{id}', name: 'decrease')]
    public function decrease(Cart $cart, $id): Response
    {
        $cart->decrease($id);
        return $this->redirectToRoute('cart_view');
    }
}
