<?php

namespace App\Controller;

use App\Classe\Cart;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/order', name: 'order_')]
class OrderController extends AbstractController
{
    #[Route('/', name: 'view')]
    public function index(Cart $cart): Response
    {
        $data = $cart->getFull();
        return $this->render('order/index.html.twig',[
            'cart' => $data,
        ]);
    }
}
