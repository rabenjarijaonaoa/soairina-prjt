<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\CommandFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BasketController extends AbstractController
{
    #[Route('/basket', name: 'basket')]
    public function index(): Response
    {
        return $this->render('basket/index.html.twig', [
            'controller_name' => 'BasketController',
        ]);
    }

    #[Route('/add-command/{id}', name: 'command')]
    public function command($id, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository(Product::class)->findOneBy(['id' => $id]);

        $form = $this->createForm(CommandFormType::class, $data);
        $form->handleRequest($request);

        return $this->render('basket/command.html.twig', [
            'product' => $data,
            'commandForm' => $form->createView(),
        ]);
    }
}
