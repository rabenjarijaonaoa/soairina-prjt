<?php

namespace App\Controller;

use App\Entity\Blogpost;
use App\Entity\Like;
use App\Entity\Product;
use App\Repository\BlogpostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class BlogpostController extends AbstractController
{
    private BlogpostRepository $blogpostRepository;

    public function __construct(BlogpostRepository $blogpostRepository)
    {
        $this->blogpostRepository = $blogpostRepository;
    }

    #[Route('/blogpost', name: 'blogpost')]
    public function blogposts(
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository(Blogpost::class)->findAllOrderByPublishedOn();
        $produits = $em->getRepository(Product::class)->findAllOrderByCreatedAt();
        $bestLike = $em->getRepository(Like::class)->getBestLikeBlogpost();
        $best = $em->getRepository(Blogpost::class)->findOneBy(['id' => $bestLike[0]['blogpost_id']]);

        $blogposts = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('blogpost/index.html.twig', [
            'blogposts' => $blogposts,
            'produits' => $produits,
            'best' => $best,
        ]);
    }

    /**
     * @Route("/blogpost/{id}", name="blogpost-id")
     */
    public function blogpost(Request $request, $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository(Blogpost::class)->findOneBy(['id' => $id]);

        return $this->render('blogpost/blog-id.html.twig', [
            'blog' => $data,
        ]);
    }
}
