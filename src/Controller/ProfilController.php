<?php

namespace App\Controller;

use App\Form\UserEditFormType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfilController extends AbstractController
{
    #[Route('/profil', name: 'profil')]
    public function index(): Response
    {
        return $this->render('profil/index.html.twig');
    }

    #[Route('/profil/edit/{id}', name: 'edit-profil')]
    public function edit($id, Request $request, FlashyNotifier $flashy): Response
    {
        $user = $this->getUser();

        $form = $this->createForm(UserEditFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $user
                ->setFirstname($form->get('firstname')->getData())
                ->setLastname($form->get('lastname')->getData())
                ->setEmail($form->get('email')->getData())
                ->setPhoneNumber($form->get('phoneNumber')->getData())
                ->setAdress($form->get('adress')->getData())
                ->setUpdateAt(new \DateTime('now'))
            ;

            // On récupère l'images transmise
            $image = $form->get('pictures')->getData();

            // On génère un nouveau nom de fichier
            $fichier = md5(uniqid()) . '.' . $image->guessExtension();

            // On copie le fichier dans le dossier uploads
            $image->move(
                $this->getParameter('profil_directory'),
                $fichier
            );

            $user->setPictures($fichier);

            $entityManager->persist($user);
            $entityManager->flush();

            $flashy->primaryDark('Profil modifié avec succès !');

            return $this->redirectToRoute('profil');

        }

        return $this->render('profil/edit.html.twig', [
            'editForm' => $form->createView(),
        ]);
    }
}
